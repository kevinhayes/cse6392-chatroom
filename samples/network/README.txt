Overview:

  Note: Utilizes "Chain of Responsibility" design pattern

  src/net/MessageHandler.java
    - A message handling class built with the Chain of Responsibility design pattern.
      The class holds a "link" (ie. the member variable "next") to another handler 
      in the "chain". Subclasses extend this class and implement the handleMessage(String) 
      abstract method. 
    - Classes that aggregate the MessageHandler class essentially have access to a 
      chain of message handling classes
    - Makes it EASY to add new classes to handle specific network messages.
 
  src/net/TestClientMessageHandler.java
    - A subclass of MessageHandler used to test messages from the server.

  src/net/TestServerMessageHandler.java
    - A subclass of MessageHandler used to test messages from the client.
   
  src/net/ClientConnectionManager.java
    - The client tcp class.
    - Spawns thread that listens for messages
    - send message functionality
    - When the client receives a message, it gets passed to the message handler. Each
      MessageHandler subclass inspects the message to determine if it supposed to 
      process the message. Otherwise, it passes the message to the next MessageHandler 
      in the chain.

  src/net/ServerConnectionManager.java
    - The server tcp class
    - Similar in functionality to the ClientConnectionManager class

  src/test/testNet.java
    - main program
    - runs client or server based on argument provided

Build instructions (w/ Apache Ant):

  Note: instructions are for use with given directory structure

  ant clean compile jar

Running instructions (w/ Apache Ant):
  
  ant run -Dargs=s (to run Server)
  ant run -Dargs=c (to run Client)

Current State:

  Running Client;
    Sends a simple test message: "CLIENT:KevinHayes:Password"

  Running Server:
    Prints received messages
    Bug: have to restart server to receive another message after first...I don't get my print statement, aaaarrrgggg!!
