package db;

/**
 * Created by William Sims on 7/8/2015.
 * This holds the user that is logged in mainly utilized for sending messages to the server and setting up chatroom
 * names.  This will expand when the server utilizes this class to know who to send messages to.
 */
public class User {
    private String user_name = null;
    private String password = null;
    private int hashPass = 0;

    public User(String name){
      user_name = name;
    }

    public void setUser_name( String name ) {
      user_name = name;
    }

    public String getUser_name(){
      return user_name;
    }

    public void setUser_pass( String passwd ) {
      password = passwd;
      hashPass = password . hashCode( );
    }

    public String getUser_pass() {
      return password;
    }

    public String getUser_hashPass() {
      return Integer.toString( hashPass );
    }   
}
