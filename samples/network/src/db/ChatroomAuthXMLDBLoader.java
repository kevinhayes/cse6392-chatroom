package db;

import db.User;
import db.DBMgrXML;
import java.io.IOException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;


/* 
 * Example XML:
 * 
 * <?xml version="1.0" encoding="UTF-8" standalone="no"?>
 * <ChatroomRegistration>
 *     <User Name="John Doe" >
 *         <Authentication Password="P@$$w0rd" /Authentication>
 *     </User>
 * </ChatroomRegistration>
 * 
 */

/**
 *  Parses chatroom authentication xml file and creates User objects.
 */
public class ChatroomAuthXMLDBLoader
{
    /** The Document object that will be used to read in the xml file */
    private Document doc;
    
    /**
     *  Loads and parses xml file using a DocumentBuilder factory object
     *  newDocumentBuilder().
     *  Empty nodes (i.e. whitespace, line breaks) in the Document object removed.
     *  
     *  Note: The Document Object Model (DOM) is an API that represents the entire document 
     *  (loaded into memory) as a tree of node objects representing the document's contents.
     */
    public ChatroomAuthXMLDBLoader (String filename) throws IOException, ParserConfigurationException, SAXException
    {
      /** Get the DOM Builder Factory */
      DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance ();

      /** Get the DOM Builder */
      DocumentBuilder builder = factory.newDocumentBuilder ();
        
      /** Load and Parse the XML document */
      doc = builder.parse(filename);
             
      try
      {
        /** Remove whitespace, carriage returns, etc. These end up 
         * being parsed as "empty nodes."
         * */
        RemoveEmptyNodes();
      } catch (XPathExpressionException e)
      {
        e.printStackTrace();
      }
      
      /** Get the parsed elements and attributes */
      GetElementsAndAttributes();
    }

    /**
     *  Parses xml file
     *  
     *  This solution (RemoveEmptyNodes()) was obtained from a forum 
     *  discussion found at the URL below (04/19/14):
     *  http://stackoverflow.com/questions/4650878/how-to-remove-text-from-my-node-parsing-in-java-dom-xml-parsing
     *  
     *  XPath provides the syntax to define part of an XML document. 
     *  XPath Expression is a query language to select part of the XML document based on the query String. 
     *  Using XPath Expressions, we can find nodes in any xml document satisfying the query string.
     */
    private void RemoveEmptyNodes() throws XPathExpressionException
    {
        XPathFactory xpathFactory = XPathFactory.newInstance();
        
        /** XPath to find empty text nodes. */
        XPathExpression xpathExp = xpathFactory.newXPath()
                .compile("//text()[normalize-space(.) = '']");
        
        NodeList emptyTextNodes = (NodeList)xpathExp.evaluate(doc, XPathConstants.NODESET);
        
        /** Remove each empty text node from document. */
        for (int i = 0; i < emptyTextNodes.getLength(); i++) {
            Node emptyTextNode = emptyTextNodes.item(i);
            emptyTextNode.getParentNode().removeChild(emptyTextNode);
        }    
    }
    
    /**
     *  Process the parsed xml elements and attributes.
     *  Create User objects
     */
    private void GetElementsAndAttributes( )
    {
        /** Get all the xml element nodes not including the document root node.
         * The root node for our defined xml file format is <ChatroomRegistration>
         * */
        NodeList nodeList = doc.getDocumentElement().getChildNodes();

        for (int i = 0; i < nodeList.getLength(); i++) 
        {
          Node node = nodeList.item(i);
          
          User user = new User("");

          /** We have encountered a <User> element. */
          if (node instanceof org.w3c.dom.Element && node.getNodeName().equals("User"))   
          {
              
            /** Get the value of the "Name" attribute of the <Class> element. */
            String name = node.getAttributes().getNamedItem("Name").getNodeValue();

            user.setUser_name( name );

            /** Get all of the children elements:
             *     Password.
             */
            NodeList childNodes = node.getChildNodes();
            
            for (int j = 0; j < childNodes.getLength(); j++) 
            {
              Node cNode = childNodes.item(j);

              /** We found a <Authentication> element */
              if (cNode instanceof org.w3c.dom.Element && 
                 (cNode.getNodeName().equals("Authentication")))
              {
                  /** Grab the password attribute */
                  String password = cNode.getAttributes().getNamedItem("Password").getNodeValue();
                  user.setUser_pass(password);
              }
            }
            DBMgrXML.getInstance().addUser(user);
          }
        }
      }  

  
}//end ChatroomAuthXMLDBLoader class

