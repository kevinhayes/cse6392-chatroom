package net;

import net.MessageHandler;
import net.ServerConnectionManager;

import db.User;
import db.DBMgrXML;

import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;

/**
 * Message Format:
 *   All messages start with 'CR'
 *
 *   CR:C:ID:NUSR:NAME:PASS 
 *
 *   CR:   All messages start with 'CR
 *   C:    Client Source
 *   ID:   Client ID for msg routing
 *   NUSR: New user request
 *   NAME: User login name
 *   PASS: User password
 */

/**
*/
public class ServerMessageHandler_NewUser extends MessageHandler
{
  /* length measured in terms of number of fields */
  public final static int NEW_USER_MSG_LEN = 6;

  public ServerMessageHandler_NewUser( ) 
  {
    ServerConnectionManager . getInstance () . addMessageHandler (this);
  }

  protected boolean handleMessage( String msg ) 
  {
    if( msg != null )
    {
      String delims = "[:]";
      String[] tokens = msg . split( delims );

      if( tokens.length >= NEW_USER_MSG_LEN )
      {
        if( tokens[ 0 ] . equals( "CR" ) && tokens[ 1 ] .equals( "C" ) )
        {
          System.out.println( "ServerMessageHandler_NewUser -- Got a client msg: " + msg.trim() );

          int id = Integer.parseInt( tokens[ 2 ] );

          if( tokens[ 3 ] . equals( "NUSR" ) )
          {
            String name = tokens[ 4 ];
            String pass = tokens[ 5 ];

            User user = new User( name );
            user . setUser_pass( pass );

            DBMgrXML . getInstance( ) . addUser( user );

            /* Send an authorization message validating new user:
             *   CR:S:AUTH:NAME */
            String outgoing = "CR:S:AUTH:" + name;
            ServerConnectionManager . getInstance () 
                                    . sendMessage ( id, outgoing );

            DBMgrXML . writeDB( );

            return true;
          }
        }
      }
      else 
      {
        System.out.println("ServerMessageHandler_NewUser -- got a msg with " + Integer.toString(tokens.length) + " tokens?");
        System.out.println("                             -- msg: " + msg );
      }
    }
    return false;
  }

} /* end ServerMessageHandler_NewUser */
