package net;

import java.net.*;
import java.io.*;

/**
 * Simple TCP Server class
 */
public class ServerConnection implements Runnable
{
  private int id;

  private MessageHandler handlers = null;

  public Socket clientSocket;

  public OutputStream outToClient;
  public DataOutputStream out;

  public InputStream inFromClient;
  public InputStreamReader in;
  //public DataInputStream in;

  public BufferedReader readClient;

  public ServerConnection( int _id, Socket client_socket, MessageHandler message_handler ) 
  {
    id = _id;
    clientSocket = client_socket;
    handlers = message_handler;

    ServerConnectionManager . getInstance () . addConnection( this );

    ( new Thread ( this ) ) . start( );
  }

  public int getId( ) { return id; }
 
  /**
   * This is the function that runs in the thread
   */
  public void run( )
  {
    System.out.println( "New communication thread started..." );

    try {
      outToClient = clientSocket.getOutputStream( );
      out = new DataOutputStream( outToClient );
      
      inFromClient = clientSocket.getInputStream( );
      in = new InputStreamReader( inFromClient );
      
      readClient = new BufferedReader( in );

    }catch(IOException e){
      System .err .println( e .toString ( ) );
    }

    while( true )
    {
      String incoming = readMessage( );

      if( handlers != null && incoming != null )
      {
        handlers . message( incoming );
      }
    }
  }

  /**
  * Send some bytes
  */
  public boolean sendMessage( String msg ) 
  {
    boolean result = false;
    try {
        out . writeUTF( msg + "\n" );
        result = true;
    }catch( IOException e ) {
      e . printStackTrace( );
    }
    return result;
  }

  /**
   * Read a message and return it
   */
  public String readMessage( ) {
//    String incoming = null;
    String msg = null;

    try {
      msg = readClient.readLine( );
      if( msg != null )
      {
        msg = msg.trim( );     
       
        if( msg . startsWith( "CR:C" ) )
        {
          /* add the server connection ID to the message in order for 
           * the message handlers to pass to the ServerConnectionManager 
           * when sending messages 
           */
          String replacement = "CR:C:" + Integer.toString( id );
          msg = msg . replaceFirst( "CR:C", replacement );
        }
      }
    }catch(IOException e){
      System .err .println( e .toString ( ) );
    }

    return msg;
  }
}
