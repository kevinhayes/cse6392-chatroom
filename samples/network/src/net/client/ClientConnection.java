package net;

import java.net.*;
import java.io.*;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * TCP Client class
 */
//public class ClientConnection implements Runnable 
public class ClientConnection implements Runnable
{
  private MessageHandler handlers = null;

  private boolean connected = false;

  private static Lock _mutex;

  public final boolean isConnected( ) 
  { 
    return connected; 
  }

  private Socket client;

  private OutputStream outToServer;
  private DataOutputStream out;

  private InputStream inFromServer;
  private InputStreamReader in;
  //public DataInputStream in;

  private BufferedReader readServer;

  public ClientConnection( Socket socket, MessageHandler msg_handler ) 
  {
    handlers = msg_handler;
    client = socket;

    _mutex = new ReentrantLock( true );

    ( new Thread ( this ) ) . start( );
  }
  
  /**
   * This is the function that runs in the thread
   */
  public void run() {

    try {
      System.out.println( "Success! Connected to "
                          + client.getRemoteSocketAddress( ) );
      connected = true;

      outToServer = client.getOutputStream( );
      out = new DataOutputStream( outToServer );
      
      inFromServer = client.getInputStream( );
      in = new InputStreamReader( inFromServer );
 
      readServer = new BufferedReader( in );
  
    }catch(IOException e){
      System .err .println( e .toString ( ) );
      disconnect( );
    }

    while( isConnected () ) 
    {
      String incoming = readMessage( );

      if( handlers != null && incoming != null )
      {
        handlers . message( incoming );
      }
    }
  }

  public void disconnect( ) 
  {
    if( !isConnected( ) ) 
      return;
    try {
      connected = false;
      client . close( );
    } catch( IOException e ) {
      e . printStackTrace( );
    }
  }

  /**
  * Send some bytes
  */
  public boolean sendMessage (String msg) 
  {
    boolean result = false;

    try {
      if( isConnected( ) )
        out . writeUTF( msg + "\n" );
        result = true;
    }catch( IOException e ) {
      e . printStackTrace( );
      disconnect( );
    }
    return result;
  }

  /**
   * Read a message and return it
   */
  public String readMessage( ) {
    String msg = null;

    try {
      msg = readServer.readLine( );
      
      if( msg != null )
        msg = msg . trim( );
    }catch(IOException e){
      if (isConnected()) e.printStackTrace( );
      disconnect( );
    }
    return msg;
  }
}
