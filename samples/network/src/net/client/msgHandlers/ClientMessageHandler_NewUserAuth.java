package net;

import net.MessageHandler;
import net.ClientConnectionManager;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;

/**
 * Message Format:
 *   All messages start with 'CR'
 *
 *   CR:S:AUTH:NAME 
 *
 *   CR:   All messages start with 'CR
 *   S:    Server Source
 *   AUTH: New user server authentication
 *   NAME: User login name
 */

/**
*/
public class ClientMessageHandler_NewUserAuth extends MessageHandler
{
  /* length measured in terms of number of fields */
  public final static int NEW_USER_AUTH_MSG_LEN = 4;

  public ClientMessageHandler_NewUserAuth( ) 
  {
    ClientConnectionManager . getInstance () . addMessageHandler (this);
  }

  protected boolean handleMessage (String msg) 
  {
    if( msg != null )
    {
      String delims = "[:]";
      String[] tokens = msg . split( delims );

      if( tokens . length >= NEW_USER_AUTH_MSG_LEN )
      {
        if( tokens[ 0 ] . equals( "CR" ) && tokens[ 1 ] . equals( "S" ) )
        {
          if( tokens[ 2 ] . equals( "AUTH" ) )
          { 
            String name = tokens[ 3 ];
            /* TODO: do something with the user auth */
            System.out.println( "ClientMessageHandler_NewUserAuth -- Got a server user auth msg for user: " + name );
            return true;
          }
        }
      }
    }
    return false;
  }
} /* end ClientMessageHandler_NewUserAuth */
