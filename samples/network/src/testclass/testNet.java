package testclass;

import net.ClientConnectionManager;
import net.ClientConnection;
import net.ClientMessageHandler_NewUserAuth;
import net.ServerMessageHandler_NewUser;

import java.io.*;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;

public class testNet 
{
  public static void main(String[] args) 
  {
    System.out.println("This is the network test\n");

    String client_or_server = "";

    if( args.length > 0 )
    {
      String choice = args[0];

      if( choice.equals( "c" ) )
        client_or_server = "Client";
      else if( choice.equals( "s" ) )
        client_or_server = "Server";
    }
           
    System.out.println( client_or_server + " is running...\n" );
 
    if( client_or_server.equals( "Client" ) )
    {
      ClientMessageHandler_NewUserAuth nuaHandler = new ClientMessageHandler_NewUserAuth( );

      ClientConnection c = ClientConnectionManager . getInstance( ) . AddClient( "127.0.0.1", 10999 );

      try {
        Thread.sleep(100);
        boolean successfulSend = false;
        while( !successfulSend )
        {
          successfulSend = c . sendMessage( "CR:C:NUSR:KatelynHayes:Password" );
        } 
      } catch (InterruptedException e) {
        System.err.println("Interrupt exception");
      }
     
    }
    else if( client_or_server.equals( "Server" ) )
    { 
      ServerMessageHandler_NewUser nuHandler = new ServerMessageHandler_NewUser( );
    }
  }
}
