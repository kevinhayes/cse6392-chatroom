package com.chatroom;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created by William Sims on 7/8/2015.
 * Main login GUI, also can create users.
 */
public class Login extends JFrame {
    private JPanel panel1;
    private JTextField userText;
    private JPasswordField passwordText;

    public Login(){
        super("Chat Application");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        panel1 = new JPanel();
        
        add(panel1);
        placeComponents(panel1);
        pack();
    }

    private void placeComponents(JPanel panel) {

        panel.setLayout(null);

        JLabel userLabel = new JLabel("User");
        userLabel.setBounds(10, 10, 80, 25);
        panel.add(userLabel);

        userText = new JTextField(20);
        userText.setBounds(100, 10, 160, 25);
        panel.add(userText);

        JLabel passwordLabel = new JLabel("Password");
        passwordLabel.setBounds(10, 40, 80, 25);
        panel.add(passwordLabel);

        passwordText = new JPasswordField(20);
        passwordText.setBounds(100, 40, 160, 25);
        panel.add(passwordText);

        JButton loginButton = new JButton("Login");
        loginButton.setBounds(10, 80, 80, 25);
        loginButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                ChatClient.getInstance().Login(userText.getText(), new String(passwordText.getPassword()));
            }
        });
        panel.add(loginButton);

        JButton registerButton = new JButton("Create User");
        registerButton.setBounds(160, 80, 120, 25);
        registerButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                ChatClient.getInstance().CreateUser(userText.getText(), new String(passwordText.getPassword()));
            }
        });
        panel.add(registerButton);

        getRootPane().setDefaultButton(loginButton);
    }

    public void pop_up(String message, String title){
        JOptionPane.showMessageDialog(null, message, "InfoBox: " + title, JOptionPane.INFORMATION_MESSAGE);
    }
}
