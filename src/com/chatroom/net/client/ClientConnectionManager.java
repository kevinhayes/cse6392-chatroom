package com.chatroom.net.client;


import com.chatroom.net.MessageHandler;

import java.net.*;
import java.io.*;
import java.util.Vector;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * TCP Client Manager class
 */
public class ClientConnectionManager
{
  private static ClientConnectionManager instance = null;
  
  private static MessageHandler handlers = null;

  /* TO DO: can keep track of multiple clients if needed */
  private Vector<ClientConnection> connections = null;

  private static Lock _mutex;

  private ClientConnectionManager( ) 
  {
    //connections = new Vector<ClientConnection> () ;
    _mutex = new ReentrantLock( true );
  }
  
  public ClientConnection AddClient( String serverName, int port )
  {
    ClientConnection cc = null;

    try {
      cc = new ClientConnection( new Socket( serverName, port ), handlers );
      // TO DO: can keep track of clients later for GUI
      //connections.add( cc );
    }catch(IOException e){
      System .err .println( e .toString ( ) );
    }
    return cc;
  }

  public static ClientConnectionManager getInstance( )
  {
    if( instance == null )
      instance = new ClientConnectionManager( );
      //instance = new ClientConnectionManager( "127.0.0.1", 10999 );
    return instance;
  }
  
  public void addMessageHandler( MessageHandler h ) 
  {
    if( handlers == null )
      handlers = h;
    else
      handlers . setNext( h );
  }
}
