package com.chatroom.net.client.msgHandlers;


/**
 * Message Format:
 *   All messages start with 'CR'
 *
 *   CR:S:NUSRCONF:NAME 
 *
 *   CR:   All messages start with 'CR
 *   S:    Server Source
 *   NUSRCONF: New user server confirmation
 *   NAME: User login name
 *   SUCC/FAIL: Login Failed or succeeded
 */

import com.chatroom.ChatClient;
import com.chatroom.net.MessageHandler;
import com.chatroom.net.client.ClientConnectionManager;

/**
*/
public class ClientMessageHandler_NewUserConf extends MessageHandler
{
  /* length measured in terms of number of fields */
  public final static int NEW_USER_CONF_MSG_LEN = 5;

  public ClientMessageHandler_NewUserConf( ) 
  {
    ClientConnectionManager. getInstance() . addMessageHandler (this);
  }

  protected boolean handleMessage (String msg) 
  {
    if( msg != null )
    {
      String delims = "[:]";
      String[] tokens = msg . split( delims );

      if( tokens . length >= NEW_USER_CONF_MSG_LEN )
      {
        if( tokens[ 0 ] . equals( "CR" ) && tokens[ 1 ] . equals( "S" ) )
        {
          if( tokens[ 2 ] . equals( "NUSRCONF" ) )
          {
            String name = tokens[ 3 ];

            if(tokens [4] . equals("SUCC"))
            {
              ChatClient.getInstance().success(name);
            }
            else
            {
              ChatClient.getInstance().failed("FAILED TO CREATE USER\n USER NAME ALREADY EXISTS", "CREATE USER ERROR");
            }

            System.out.println( "ClientMessageHandler_NewUserConf -- Got a server new user confirmation msg for user: " + name );
            return true;
          }
        }
      }
    }
    return false;
  }
} /* end ClientMessageHandler_NewUserConf */
