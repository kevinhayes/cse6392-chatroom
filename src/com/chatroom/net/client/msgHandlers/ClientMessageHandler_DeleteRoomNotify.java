package com.chatroom.net.client.msgHandlers;


/**
 * Message Format:
 *   All messages start with 'CR'
 *
 *   CR:S:DRMNFY:NAME 
 *
 *   CR:     All messages start with 'CR
 *   S:      Server Source
 *   DRMNFY: Chat room deletion notification
 *   NAME:   Chat room name
 */

import com.chatroom.ChatClient;
import com.chatroom.net.MessageHandler;
import com.chatroom.net.client.ClientConnectionManager;

/**
*/
public class ClientMessageHandler_DeleteRoomNotify extends MessageHandler
{
  /* length measured in terms of number of fields */
  public final static int DELETE_ROOM_NOTIFY_MSG_LEN = 4;

  public ClientMessageHandler_DeleteRoomNotify( ) 
  {
    ClientConnectionManager. getInstance() . addMessageHandler (this);
  }

  protected boolean handleMessage (String msg) 
  {
    if( msg != null )
    {
      String delims = "[:]";
      String[] tokens = msg . split( delims );

      if( tokens . length >= DELETE_ROOM_NOTIFY_MSG_LEN )
      {
        if( tokens[ 0 ] . equals( "CR" ) && tokens[ 1 ] . equals( "S" ) )
        {
          if( tokens[ 2 ] . equals( "DRMNFY" ) )
          { 
            String room_name = tokens[ 3 ];

            ChatClient.getInstance().removeChatroom(room_name);

            System.out.println( "ClientMessageHandler_DeleteRoomNotify -- Got a server delete chatroom notification msg for room: " + room_name );
            return true;
          }
        }
      }
    }
    return false;
  }
} /* end ClientMessageHandler_DeleteRoomNotify */
