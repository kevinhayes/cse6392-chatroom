package com.chatroom.net.client.msgHandlers;


/**
 * Message Format:
 *   All messages start with 'CR'
 *
 *   CR:S:LINCONF:NAME 
 *
 *   CR:   All messages start with 'CR
 *   S:    Server Source
 *   LINCONF: Login server confirmation
 *   NAME: User login name
 *   SUCC/FAIL: Login Failed or succeeded
 */

import com.chatroom.ChatClient;
import com.chatroom.net.MessageHandler;
import com.chatroom.net.client.ClientConnectionManager;

/**
*/
public class ClientMessageHandler_LoginConf extends MessageHandler
{
  /* length measured in terms of number of fields */
  public final static int LOGIN_CONF_MSG_LEN = 5;

  public ClientMessageHandler_LoginConf()
  {
    ClientConnectionManager. getInstance() . addMessageHandler (this);
  }

  protected boolean handleMessage (String msg) 
  {
    if( msg != null )
    {
      String delims = "[:]";
      String[] tokens = msg . split( delims );

      if( tokens . length >= LOGIN_CONF_MSG_LEN )
      {
        if( tokens[ 0 ] . equals( "CR" ) && tokens[ 1 ] . equals( "S" ) )
        {
          if( tokens[ 2 ] . equals( "LINCONF" ) )
          { 
            String name = tokens[ 3 ];

            if(tokens [4] . equals("SUCC"))
            {
              ChatClient.getInstance().success(name);
            }
            else
            {
              ChatClient.getInstance().failed("FAILED TO LOGIN", "LOGIN ERROR");
            }

            System.out.println( "ClientMessageHandler_LoginConf -- Got a server login confirmation msg for user: " + name );
            return true;
          }
        }
      }
    }
    return false;
  }
} /* end ClientMessageHandler_LoginConf */
