package com.chatroom.net.client.msgHandlers;


/**
 * Message Format:
 *   All messages start with 'CR'
 *
 *   CR:S:LOUTCONF:NAME 
 *
 *   CR:   All messages start with 'CR
 *   S:    Server Source
 *   LOUTCONF: Logout server confirmation
 *   NAME: User login name
 */

import com.chatroom.net.MessageHandler;
import com.chatroom.net.client.ClientConnectionManager;

/**
*/
public class ClientMessageHandler_LogoutConf extends MessageHandler
{
  /* length measured in terms of number of fields */
  public final static int LOGOUT_CONF_MSG_LEN = 4;

  public ClientMessageHandler_LogoutConf()
  {
    ClientConnectionManager. getInstance() . addMessageHandler (this);
  }

  protected boolean handleMessage (String msg) 
  {
    if( msg != null )
    {
      String delims = "[:]";
      String[] tokens = msg . split( delims );

      if( tokens . length >= LOGOUT_CONF_MSG_LEN )
      {
        if( tokens[ 0 ] . equals( "CR" ) && tokens[ 1 ] . equals( "S" ) )
        {
          if( tokens[ 2 ] . equals( "LOUTCONF" ) )
          { 
            String name = tokens[ 3 ];

            System.out.print("Logout CALLED\n");

            System.out.println("ClientMessageHandler_LogoutConf -- Got a server Logout confirmation msg for user: " + name);
            System.exit(1);

            return true;

          }
        }
      }
    }
    return false;
  }
} /* end ClientMessageHandler_LogoutConf */
