package com.chatroom.net.client.msgHandlers;


/**
 * Message Format:
 *   All messages start with 'CR'
 *
 *   CR:S:NRMNFY:NAME 
 *
 *   CR:     All messages start with 'CR
 *   S:      Server Source
 *   RMSNFY: Chat room join notification
 *   CNAME:  Chat room names
 */

import com.chatroom.ChatClient;
import com.chatroom.net.MessageHandler;
import com.chatroom.net.client.ClientConnectionManager;

/**
*/
public class ClientMessageHandler_GetRoomsNotify extends MessageHandler
{
  /* length measured in terms of number of fields */
  public final static int GET_ROOM_NOTIFY_MSG_LEN = 3;

  public ClientMessageHandler_GetRoomsNotify()
  {
    ClientConnectionManager. getInstance() . addMessageHandler (this);
  }

  protected boolean handleMessage (String msg) 
  {
    if( msg != null )
    {
      String delims = "[:]";
      String[] tokens = msg . split( delims );

      if( tokens . length >= GET_ROOM_NOTIFY_MSG_LEN )
      {
        if( tokens[ 0 ] . equals( "CR" ) && tokens[ 1 ] . equals( "S" ) )
        {
          if( tokens[ 2 ] . equals( "RMSNFY" ) )
          { 
            for ( int i = 3; i < tokens.length; i++)
            {
              ChatClient.getInstance().AddChatroom(tokens[i]);
            }

            ChatClient.getInstance().createChatroomGui();

            System.out.println( "ClientMessageHandler_GetRoomNotify -- Got a server chatroom get notify "  );
            return true;
          }
        }
      }
    }
    return false;
  }
} /* end ClientMessageHandler_JoinRoomNotify */
