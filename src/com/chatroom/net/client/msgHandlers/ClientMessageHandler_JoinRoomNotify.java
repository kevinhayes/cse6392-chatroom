package com.chatroom.net.client.msgHandlers;


/**
 * Message Format:
 *   All messages start with 'CR'
 *
 *   CR:S:NRMNFY:NAME 
 *
 *   CR:     All messages start with 'CR
 *   S:      Server Source
 *   JRMNFY: Chat room join notification
 *   CNAME:  Chat room name
 *   UNAME:  User name
 */

import com.chatroom.ChatClient;
import com.chatroom.ChatGUI;
import com.chatroom.ChatServer;
import com.chatroom.net.MessageHandler;
import com.chatroom.net.client.ClientConnectionManager;

/**
*/
public class ClientMessageHandler_JoinRoomNotify extends MessageHandler
{
  /* length measured in terms of number of fields */
  public final static int JOIN_ROOM_NOTIFY_MSG_LEN = 5;

  public ClientMessageHandler_JoinRoomNotify( ) 
  {
    ClientConnectionManager. getInstance() . addMessageHandler (this);
  }

  protected boolean handleMessage (String msg) 
  {
    if( msg != null )
    {
      String delims = "[:]";
      String[] tokens = msg . split( delims );

      if( tokens . length >= JOIN_ROOM_NOTIFY_MSG_LEN )
      {
        if( tokens[ 0 ] . equals( "CR" ) && tokens[ 1 ] . equals( "S" ) )
        {
          if( tokens[ 2 ] . equals( "JRMNFY" ) )
          { 
            String room_name = tokens[ 3 ];
            String user_name = tokens[ 4 ];

            if(ChatClient.getInstance().getUser().getUser_name().equals(user_name)) {
              ChatClient.getInstance().ChatroomJoined(room_name);
            }

            for( int i = 0; i < ChatClient.getInstance().getChatrooms().size(); i++) {
              if(ChatClient.getInstance().getChatrooms().get(i).getName().equals(room_name)) {
                String message = user_name + " has just joined " + room_name + "!";
                ChatClient.getInstance().displayMessage(message, room_name);
              }
            }

            System.out.println( "ClientMessageHandler_JoinRoomNotify -- Got a server chatroom join notification msg for room: " + room_name );
            return true;
          }
        }
      }
    }
    return false;
  }
} /* end ClientMessageHandler_JoinRoomNotify */
