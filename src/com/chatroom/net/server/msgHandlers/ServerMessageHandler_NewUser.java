package com.chatroom.net.server.msgHandlers;

import com.chatroom.ChatServer;
import com.chatroom.db.DBMgrXML;
import com.chatroom.db.User;
import com.chatroom.net.MessageHandler;
import com.chatroom.net.server.ServerConnectionManager;


/**
 * Message Format:
 *   All messages start with 'CR'
 *
 *   CR:C:ID:NUSR:NAME:PASS 
 *
 *   CR:   All messages start with 'CR
 *   C:    Client Source
 *   ID:   Client ID for msg routing
 *   NUSR: New user request
 *   NAME: User login name
 *   PASS: User password
 */

/**
*/
public class ServerMessageHandler_NewUser extends MessageHandler
{
  /* length measured in terms of number of fields */
  public final static int NEW_USER_MSG_LEN = 6;

  public ServerMessageHandler_NewUser( ) 
  {
    ServerConnectionManager. getInstance() . addMessageHandler (this);
  }

  protected boolean handleMessage( String msg ) 
  {
    if( msg != null )
    {
      String delims = "[:]";
      String[] tokens = msg . split( delims );

      if( tokens.length >= NEW_USER_MSG_LEN )
      {
        if( tokens[ 0 ] . equals( "CR" ) && tokens[ 1 ] .equals( "C" ) )
        {
          System.out.println( "ServerMessageHandler_NewUser -- Got a client msg: " + msg.trim() );

          int id = Integer.parseInt( tokens[ 2 ] );

          if( tokens[ 3 ] . equals( "NUSR" ) )
          {
            String name = tokens[ 4 ];
            String pass = tokens[ 5 ];

            /* Send an authorization message validating new user:
               *   CR:S:AUTH:NAME */
            String outgoing = "CR:S:NUSRCONF:" + name;
            if ( DBMgrXML . getInstance() . checkUserName(name))
            {
              User user = new User(name);

              String encryptedPassword = DBMgrXML . getInstance() . encryptUserPass( pass );
              user.setUser_pass(encryptedPassword);

              DBMgrXML.getInstance().addUser(user);
              ChatServer.getInstance().addUser(user);
              user.setUser_id(id);

              /* Send an authorization message validating new user:
               *   CR:S:AUTH:NAME:SUCC */
              outgoing += ":SUCC";

              DBMgrXML.writeDB();
            }
            else
            {
              outgoing += ":FAIL";
            }

            System.out.print(outgoing);

            ServerConnectionManager.getInstance()
                    .sendMessage(id, outgoing);

            return true;
          }
        }
      }
    }
    return false;
  }

} /* end ServerMessageHandler_NewUser */
