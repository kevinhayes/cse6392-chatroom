package com.chatroom.net.server.msgHandlers;

import com.chatroom.ChatServer;
import com.chatroom.Chatroom;
import com.chatroom.db.DBMgrXML;
import com.chatroom.db.User;
import com.chatroom.net.MessageHandler;
import com.chatroom.net.server.ServerConnectionManager;


/**
 * Incoming Message Format:
 *   All messages start with 'CR'
 *
 *   CR:C:ID:JRM:NAME
 *
 *   CR:   All messages start with 'CR
 *   C:    Client Source
 *   ID:   Client ID for msg routing
 *   JRM:  Join chat room request
 *   NAME: Name of the chat room
 */

/**
*/
public class ServerMessageHandler_JoinRoom extends MessageHandler
{
  /* length measured in terms of number of fields */
  public final static int JOIN_ROOM_MSG_LEN = 5;

  public ServerMessageHandler_JoinRoom( ) 
  {
    ServerConnectionManager. getInstance() . addMessageHandler (this);
  }

  protected boolean handleMessage( String msg ) 
  {
    if( msg != null )
    {
      String delims = "[:]";
      String[] tokens = msg . split( delims );

      if( tokens.length >= JOIN_ROOM_MSG_LEN )
      {
        if( tokens[ 0 ] . equals( "CR" ) && tokens[ 1 ] .equals( "C" ) )
        {
          System.out.println( "ServerMessageHandler_JoinRoom -- Got a client msg: " + msg.trim() );

          int id = Integer.parseInt( tokens[ 2 ] );

          if( tokens[ 3 ] . equals( "JRM" ) )
          {
            String room_name = tokens[ 4 ];

            if( ChatServer . getInstance( ) . joinChatroom( id, room_name ) )
            {
              User u = ChatServer . getInstance( ) . getUser( id );
              String user_name = u . getUser_name( );

              /* Send a notification message to clients of successful join:
               *   CR:S:JRMNFY:NAME */
              String outgoing = "CR:S:JRMNFY:" + room_name + ":" + user_name;
              ServerConnectionManager.getInstance()
                                     .sendAllMessage(id, outgoing);

              return true;
            }
          }
        }
      }
    }
    return false;
  }

} /* end ServerMessageHandler_JoinRoom */
