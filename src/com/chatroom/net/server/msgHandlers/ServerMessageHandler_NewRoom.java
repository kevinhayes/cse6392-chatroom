package com.chatroom.net.server.msgHandlers;

import com.chatroom.ChatServer;
import com.chatroom.Chatroom;
import com.chatroom.db.DBMgrXML;
import com.chatroom.db.User;
import com.chatroom.net.MessageHandler;
import com.chatroom.net.server.ServerConnectionManager;


/**
 * Incoming Message Format:
 *   All messages start with 'CR'
 *
 *   CR:C:ID:NRM:NAME
 *
 *   CR:   All messages start with 'CR
 *   C:    Client Source
 *   ID:   Client ID for msg routing
 *   NRM:  New chat room request
 *   NAME: Name of the chat room
 */

/**
*/
public class ServerMessageHandler_NewRoom extends MessageHandler
{
  /* length measured in terms of number of fields */
  public final static int NEW_ROOM_MSG_LEN = 5;

  public ServerMessageHandler_NewRoom( ) 
  {
    ServerConnectionManager. getInstance() . addMessageHandler (this);
  }

  protected boolean handleMessage( String msg ) 
  {
    if( msg != null )
    {
      String delims = "[:]";
      String[] tokens = msg . split( delims );

      if( tokens.length >= NEW_ROOM_MSG_LEN )
      {
        if( tokens[ 0 ] . equals( "CR" ) && tokens[ 1 ] .equals( "C" ) )
        {
          System.out.println( "ServerMessageHandler_NewRoom -- Got a client msg: " + msg.trim() );

          int id = Integer.parseInt( tokens[ 2 ] );

          if( tokens[ 3 ] . equals( "NRM" ) )
          {
            String room_name = tokens[ 4 ];

            if ( ChatServer . getInstance() . addChatroom(new Chatroom( room_name )) )
            {
              /* Send a notification message to all clients of a new chat room:
               *   CR:S:NRMNFY:NAME */
              String outgoing = "CR:S:NRMNFY:" + room_name;

              ServerConnectionManager.getInstance()
                                     .sendAllMessage(id, outgoing);

            }
            return true;
          }
        }
      }
    }
    return false;
  }

} /* end ServerMessageHandler_NewRoom */
