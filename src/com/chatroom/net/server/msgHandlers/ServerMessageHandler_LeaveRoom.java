package com.chatroom.net.server.msgHandlers;

import com.chatroom.ChatServer;
import com.chatroom.Chatroom;
import com.chatroom.db.DBMgrXML;
import com.chatroom.db.User;
import com.chatroom.net.MessageHandler;
import com.chatroom.net.server.ServerConnectionManager;


/**
 * Incoming Message Format:
 *   All messages start with 'CR'
 *
 *   CR:C:ID:LRM:NAME
 *
 *   CR:   All messages start with 'CR
 *   C:    Client Source
 *   ID:   Client ID for msg routing
 *   LRM:  Leave chat room request
 *   NAME: Name of the chat room
 */

/**
*/
public class ServerMessageHandler_LeaveRoom extends MessageHandler
{
  /* length measured in terms of number of fields */
  public final static int LEAVE_ROOM_MSG_LEN = 5;

  public ServerMessageHandler_LeaveRoom( ) 
  {
    ServerConnectionManager. getInstance() . addMessageHandler (this);
  }

  protected boolean handleMessage( String msg ) 
  {
    if( msg != null )
    {
      String delims = "[:]";
      String[] tokens = msg . split( delims );

      if( tokens.length >= LEAVE_ROOM_MSG_LEN )
      {
        if( tokens[ 0 ] . equals( "CR" ) && tokens[ 1 ] .equals( "C" ) )
        {
          System.out.println( "ServerMessageHandler_LeaveRoom -- Got a client msg: " + msg.trim() );

          int id = Integer.parseInt( tokens[ 2 ] );

          if( tokens[ 3 ] . equals( "LRM" ) )
          {
            String room_name = tokens[ 4 ];
            User user = ChatServer.getInstance().getUser(id);

            if( ChatServer . getInstance( ) . leaveChatroom( id, room_name ) )
            {
              /* Send a notification message client of leave:
               *   CR:S:LRMNFY:NAME */
              String outgoing = "CR:S:LRMNFY:" + room_name + ":" + user.getUser_name();
              ServerConnectionManager.getInstance()
                                     .sendAllMessage(id, outgoing);

              if(ChatServer.getInstance().ChatroomEmpty(room_name)) {
                /* Send a notification message chatroom of deletion:
                 *   CR:S:LRMNFY:NAME */
                outgoing = "CR:S:DRMNFY:" + room_name;
                ServerConnectionManager.getInstance()
                        .sendAllMessage(id, outgoing);
              }
            }
            return true;
          }
        }
      }
    }
    return false;
  }

} /* end ServerMessageHandler_LeaveRoom */
