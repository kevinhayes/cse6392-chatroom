package com.chatroom.net.server.msgHandlers;

import com.chatroom.ChatClient;
import com.chatroom.ChatServer;
import com.chatroom.db.User;
import com.chatroom.net.MessageHandler;
import com.chatroom.net.server.ServerConnectionManager;


/**
 * Incoming Message Format:
 *   All messages start with 'CR'
 *
 *   CR:C:ID:JRM:NAME
 *
 *   CR:   All messages start with 'CR
 *   C:    Client Source
 *   ID:   Client ID for msg routing
 *   GETRMS:  Get chat rooms request
 */

/**
*/
public class ServerMessageHandler_GetRooms extends MessageHandler
{
  /* length measured in terms of number of fields */
  public final static int GET_ROOMS_MSG_LEN = 4;

  public ServerMessageHandler_GetRooms()
  {
    ServerConnectionManager. getInstance() . addMessageHandler (this);
  }

  protected boolean handleMessage( String msg ) 
  {
    if( msg != null )
    {
      String delims = "[:]";
      String[] tokens = msg . split( delims );

      if( tokens.length >= GET_ROOMS_MSG_LEN )
      {
        if( tokens[ 0 ] . equals( "CR" ) && tokens[ 1 ] .equals( "C" ) )
        {
          System.out.println( "ServerMessageHandler_JoinRoom -- Got a client msg: " + msg.trim() );

          int id = Integer.parseInt( tokens[ 2 ] );

          if( tokens[ 3 ] . equals( "GETRMS" ) )
          {
              /* Send a notification message to clients of successful join:
               *   CR:S:JRMNFY:NAME */
              String outgoing = "CR:S:RMSNFY";

              for( int i = 0; i < ChatServer.getInstance().getChatrooms().size(); i++) {
                  outgoing += ":" + ChatServer.getInstance().getChatrooms().get(i).getName();
              }


              ServerConnectionManager.getInstance()
                                     .sendMessage(id, outgoing);

            return true;
          }
        }
      }
    }
    return false;
  }

} /* end ServerMessageHandler_GetRooms */
