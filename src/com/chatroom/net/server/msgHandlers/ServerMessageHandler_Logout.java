package com.chatroom.net.server.msgHandlers;

import com.chatroom.ChatServer;
import com.chatroom.db.DBMgrXML;
import com.chatroom.db.User;
import com.chatroom.net.MessageHandler;
import com.chatroom.net.server.ServerConnectionManager;


/**
 * Message Format:
 *   All messages start with 'CR'
 *
 *   CR:C:ID:LOUT:NAME:PASS 
 *
 *   CR:   All messages start with 'CR
 *   C:    Client Source
 *   ID:   Client ID for msg routing
 *   LOUT: New user request
 *   NAME: User login name
 */

/**
*/
public class ServerMessageHandler_Logout extends MessageHandler
{
  /* length measured in terms of number of fields */
  public final static int LOGOUT_MSG_LEN = 5;

  public ServerMessageHandler_Logout()
  {
    ServerConnectionManager. getInstance() . addMessageHandler (this);
  }

  protected boolean handleMessage( String msg ) 
  {
    if( msg != null )
    {
      String delims = "[:]";
      String[] tokens = msg . split( delims );

      if( tokens.length >= LOGOUT_MSG_LEN )
      {
        if( tokens[ 0 ] . equals( "CR" ) && tokens[ 1 ] .equals( "C" ) )
        {
          System.out.println( "ServerMessageHandler_Logout -- Got a client msg: " + msg.trim() );

          int id = Integer.parseInt( tokens[ 2 ] );

          if( tokens[ 3 ] . equals( "LOUT" ) )
          {
            String name = tokens[ 4 ];

            ChatServer.getInstance().removeUser(name);

            /* Send an authorization message validating new user:
             *   CR:S:LOUTCONF:NAME */
            String outgoing = "CR:S:LOUTCONF:" + name;
            ServerConnectionManager . getInstance () 
                                    . sendMessage ( id, outgoing );

            ServerConnectionManager . getInstance ()
                                    . closeConnection( id );

            return true;
          }
        }
      }
    }
    return false;
  }

} /* end ServerMessageHandler_Logout */
