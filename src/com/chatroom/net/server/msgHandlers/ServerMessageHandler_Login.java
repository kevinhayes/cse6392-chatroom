package com.chatroom.net.server.msgHandlers;

import com.chatroom.ChatServer;
import com.chatroom.db.DBMgrXML;
import com.chatroom.db.User;
import com.chatroom.net.MessageHandler;
import com.chatroom.net.server.ServerConnectionManager;


/**
 * Message Format:
 *   All messages start with 'CR'
 *
 *   CR:C:ID:LIN:NAME:PASS 
 *
 *   CR:   All messages start with 'CR
 *   C:    Client Source
 *   ID:   Client ID for msg routing
 *   LIN: New user request
 *   NAME: User login name
 *   PASS: User password
 */

/**
*/
public class ServerMessageHandler_Login extends MessageHandler
{
  /* length measured in terms of number of fields */
  public final static int LOGIN_MSG_LEN = 6;

  public ServerMessageHandler_Login()
  {
    ServerConnectionManager. getInstance() . addMessageHandler (this);
  }

  protected boolean handleMessage( String msg ) 
  {
    if( msg != null )
    {
      String delims = "[:]";
      String[] tokens = msg . split( delims );

      if( tokens.length >= LOGIN_MSG_LEN )
      {
        if( tokens[ 0 ] . equals( "CR" ) && tokens[ 1 ] .equals( "C" ) )
        {
          System.out.println( "ServerMessageHandler_Login -- Got a client msg: " + msg.trim() );

          int id = Integer.parseInt( tokens[ 2 ] );

          if( tokens[ 3 ] . equals( "LIN" ) )
          {
            String name = tokens[ 4 ];
            String pass = tokens[ 5 ];


            String outgoing = "CR:S:LINCONF:" + name;

            if( DBMgrXML . getInstance( ) . validateUser( name, pass ) )
            {
              User user = new User( name );
              user.setUser_id(id);

              ChatServer.getInstance().addUser(user);

              outgoing += ":SUCC";
            }
            else
              outgoing += ":FAIL";
            
            /* Send an authorization message validating user:
             *   CR:S:LINCONF:NAME */
            ServerConnectionManager . getInstance () 
                                    . sendMessage(id, outgoing);

            return true;
          }
        }
      }
    }
    return false;
  }

} /* end ServerMessageHandler_Login */
