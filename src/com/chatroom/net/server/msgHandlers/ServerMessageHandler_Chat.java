package com.chatroom.net.server.msgHandlers;

import com.chatroom.ChatServer;
import com.chatroom.Chatroom;
import com.chatroom.db.DBMgrXML;
import com.chatroom.db.User;
import com.chatroom.net.MessageHandler;
import com.chatroom.net.server.ServerConnectionManager;


/**
 * Incoming Message Format:
 *   All messages start with 'CR'
 *
 *   CR:C:ID:CHAT:MSG
 *
 *   CR:   All messages start with 'CR
 *   C:    Client Source
 *   ID:   Client ID for msg routing
 *   CHAT: Chat message
 *   ROOM: Chatroom name
 *   MSG:  The actual message
 */

/**
*/
public class ServerMessageHandler_Chat extends MessageHandler
{
  /* length measured in terms of number of fields */
  public final static int CHAT_MSG_LEN = 6;

  public ServerMessageHandler_Chat( ) 
  {
    ServerConnectionManager. getInstance() . addMessageHandler (this);
  }

  protected boolean handleMessage( String msg ) 
  {
    if( msg != null )
    {
      String delims = "[:]";
      String[] tokens = msg . split( delims );

      if( tokens.length >= CHAT_MSG_LEN )
      {
        if( tokens[ 0 ] . equals( "CR" ) && tokens[ 1 ] .equals( "C" ) )
        {
          System.out.println( "ServerMessageHandler_Chat -- Got a client msg: " + msg.trim() );

          int id = Integer.parseInt( tokens[ 2 ] );

          if( tokens[ 3 ] . equals( "CHAT" ) )
          {
            String room_name = tokens[ 4 ];
            String chat_msg = tokens[ 5 ];

            /* Send a notification message to all clients of a new chat room:
             *   CR:S:NRMNFY:NAME */
            String outgoing = "CR:S:CHATNFY:" + room_name +":" + chat_msg;

            ServerConnectionManager.getInstance()
                                   .sendAllButMessage(id, outgoing);

            return true;
          }
        }
      }
    }
    return false;
  }

} /* end ServerMessageHandler_Chat */
