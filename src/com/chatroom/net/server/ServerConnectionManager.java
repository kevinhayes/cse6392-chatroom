package com.chatroom.net.server;

import com.chatroom.db.DBMgrXML;
import com.chatroom.net.MessageHandler;

import java.net.*;
import java.io.*;
import java.util.Vector;
import java.util.Iterator;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * Simple TCP Server class
 */
public class ServerConnectionManager implements Runnable
{
  private static ServerConnectionManager instance = null;
  
  private static MessageHandler handlers = null;

  private static Vector<ServerConnection> connections = null;

  private static int nextID = -1;

  public ServerSocket serverSocket;

  private static Lock _mutex;

  private ServerConnectionManager( int server_port ) 
  {
    boolean dbLoad = DBMgrXML. getInstance() . initialize( );

    if( !dbLoad )
      return;

    _mutex = new ReentrantLock( true );

    connections = new Vector<ServerConnection>( );

    try {
      System.out.println( "Server opening connection socket on port " + server_port );

      serverSocket = new ServerSocket( server_port );
      System.out.println( "Server successfully opened connection socket on port " + server_port );

      ( new Thread ( this ) ) . start( );
    }catch(IOException e){
      System .err .println( e .toString ( ) );
    }
  }
  
  public static ServerConnectionManager getInstance( )
  {
    if( instance == null )
      instance = new ServerConnectionManager( 10999 );
    return instance;
  }
  
  public void addMessageHandler( MessageHandler h ) 
  {
    if( handlers == null )
      handlers = h;
    else
      handlers . setNext( h );
  }

  protected boolean addConnection( ServerConnection sc )
  {
    if( sc != null )
    {
      _mutex . lock( );

      connections . add( sc );

      _mutex . unlock( );

      return true;
    }
    return false;
  }

  public void closeConnection( int id )
  {
    ServerConnection sc = null;

    for ( int i = 0; i < connections.size(); i++)
    {
      if(connections.get(i).getId() == id) {
        sc = connections.get(i);
        sc.disconnect();
        connections.remove(i);
        return;
      }
    }
  }

  /**
  * Send a message to a particular client 
  */
  public boolean sendMessage( int id, String msg ) 
  {
    boolean result = false;
    ServerConnection sc = null;

    _mutex . lock( );

    Iterator<ServerConnection> it = connections . iterator( );
   
    while( it . hasNext( ) )
    {
      sc = it . next( );
      if ( sc . getId( ) == id )
        result = sc . sendMessage( msg );
    }
    
    _mutex . unlock();

    return result;    
  }

  /**
   * Send a message to all clients
   */
  public void sendAllMessage( int id, String msg )
  {
    ServerConnection sc = null;

    _mutex . lock( );

    Iterator<ServerConnection> it = connections . iterator( );

    while( it . hasNext( ) )
    {
      sc = it . next( );

      sc . sendMessage( msg );
    }

    _mutex . unlock( );
  }

  /**
  * Send a message to all clients 
  */
  public void sendAllButMessage( int id, String msg ) 
  {
    ServerConnection sc = null;

    _mutex . lock( );

    Iterator<ServerConnection> it = connections . iterator( );
   
    while( it . hasNext( ) )
    {
      sc = it . next( );

      if ( sc . getId( ) != id )
        sc . sendMessage( msg );
    }
    
    _mutex . unlock( );
  }

  /**
   * This is the function that runs in the thread
   */
  public void run( )
  {
    try { 
      while (true)
      {
        System.out.println("Waiting for connection...");
        ServerConnection sc = new ServerConnection( ++nextID, serverSocket . accept( ), handlers ); 
      }
    }
    catch( IOException e ) {
      System . err . println( "Accept failed." ); 
      System . exit( 1 ); 
    }
  }
}
