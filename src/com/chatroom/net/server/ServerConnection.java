package com.chatroom.net.server;

import com.chatroom.net.MessageHandler;

import java.net.*;
import java.io.*;

/**
 * Simple TCP Server class
 */
public class ServerConnection implements Runnable
{
  private int id;
  private boolean connected = false;
  private MessageHandler handlers = null;

  public Socket clientSocket;

  public OutputStream outToClient;
  public DataOutputStream out;

  public InputStream inFromClient;
  public InputStreamReader in;
  //public DataInputStream in;

  public BufferedReader readClient;

  public ServerConnection( int _id, Socket client_socket, MessageHandler message_handler ) 
  {
    id = _id;
    clientSocket = client_socket;
    handlers = message_handler;
    connected = true;

    ServerConnectionManager . getInstance () . addConnection( this );

    ( new Thread ( this ) ) . start( );
  }

  public int getId( ) { return id; }
 
  public void disconnect( )
  {
    connected = false;
  }

  /**
   * This is the function that runs in the thread
   */
  public void run( )
  {
    try {
      outToClient = clientSocket.getOutputStream( );
      out = new DataOutputStream( outToClient );
      
      inFromClient = clientSocket.getInputStream( );
      in = new InputStreamReader( inFromClient );
      
      readClient = new BufferedReader( in );

      while( connected )
      {
        String incoming = readMessage( );

        if( handlers != null && incoming != null )
        {
          handlers . message( incoming );
        }
      }

      readClient.close(); 
      clientSocket.close();

    }catch(IOException e){
      System .err .println( e .toString ( ) );
    }
  }

  /**
  * Send some bytes
  */
  public boolean sendMessage( String msg ) 
  {
    boolean result = false;
    try {
        out . writeUTF( msg + "\n" );
        result = true;
    }catch( IOException e ) {
      e . printStackTrace( );
    }
    return result;
  }

  /**
   * Read a message and return it
   */
  public String readMessage( ) {
//    String incoming = null;
    String msg = null;

    try {
      msg = readClient.readLine( );
      if( msg != null )
      {
        msg = msg.trim( );     
       
        if( msg . startsWith( "CR:C" ) )
        {
          /* add the server connection ID to the message in order for 
           * the message handlers to pass to the ServerConnectionManager 
           * when sending messages 
           */
          String replacement = "CR:C:" + Integer.toString( id );
          msg = msg . replaceFirst( "CR:C", replacement );
        }
      }
    }catch(IOException e){
      System .err .println( e .toString ( ) );
    }

    return msg;
  }
}
