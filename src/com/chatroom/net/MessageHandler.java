package com.chatroom.net;

public abstract class MessageHandler
{
  // The next element in the chain of responsibility
  private MessageHandler next;

  public MessageHandler( ) { }

  public void setNext(MessageHandler handler) 
  {
    if( next == null )
      next = handler;
    else
      next . setNext( handler );
  }

  public void message(String msg)
  {
    if( !handleMessage( msg ) && next != null ) 
      next . message( msg );
     
  }

  abstract protected boolean handleMessage( String msg );
}
