package com.chatroom;

import javax.swing.*;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

/**
 * Created by William Sims on 7/22/2015.
 * The GUI for the chatroom.
 */
public class ChatGUI extends JFrame {
    private String name;
    private JPanel panel1;
    private JTextPane textPane1;
    private JTextField message;
    private JButton send_button;
    private JSplitPane splitter;
    private JScrollPane scroller;

    public ChatGUI(String chatroom_name) {
        super(chatroom_name);

        // removes the gui from the client
        this.addWindowListener(new WindowAdapter()
        {
            public void windowClosing(WindowEvent e)
            {
                ChatClient.getInstance().LeaveChatroom(name);
            }
        });

        name = chatroom_name;
        splitter = new JSplitPane();
        send_button = new JButton();
        scroller = new JScrollPane();
        panel1 = new JPanel();
        textPane1 = new JTextPane();
        message = new JTextField();

        textPane1.setPreferredSize(new Dimension(300, 300));
        textPane1.setEditable(false);
        textPane1.setEnabled(false);
        textPane1.setBackground(Color.white);
        textPane1.setDisabledTextColor(Color.black);

        scroller.setViewportView(textPane1);
        scroller.setAutoscrolls(true);
        scroller.setEnabled(true);
        
        scroller.setPreferredSize(new Dimension(300, 300));
        
        message.setPreferredSize(new Dimension(200, 25));
        panel1.setPreferredSize(new Dimension(250, 30));
        
        send_button.setText("Send");
        
        panel1.add(message);
        panel1.add(send_button);
        
        splitter.setTopComponent(scroller);
        splitter.setBottomComponent(panel1);
        splitter.setOrientation(JSplitPane.VERTICAL_SPLIT);
        splitter.setResizeWeight(1);
        splitter.setDividerSize(1);
        splitter.setDividerLocation(300);
        
        add(splitter);

        setVisible(true);
        validate();
        repaint();
        pack();

        getRootPane().setDefaultButton(send_button);
        send_button.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if(message.getText().length() != 0) {
                    ChatClient.getInstance().SendMessage(message.getText(), name);
                    message.setText("");
                }
            }
        });
    }

    public String getChatroomName(){
        return name;
    }

    public void UpdateMessages(String message) {
        textPane1.setText(textPane1.getText() + "\n" + message);
    }
}
