package com.chatroom;

import com.chatroom.net.MessageHandler;
import com.chatroom.net.client.*;
import com.chatroom.net.client.msgHandlers.*;
import com.chatroom.db.User;
import com.chatroom.net.client.msgHandlers.ClientMessageHandler_LoginConf;
import com.chatroom.net.client.msgHandlers.ClientMessageHandler_LogoutConf;
import com.chatroom.net.client.msgHandlers.ClientMessageHandler_NewUserConf;
import com.chatroom.net.client.msgHandlers.ClientMessageHandler_NewRoomNotify;
import com.chatroom.net.client.msgHandlers.ClientMessageHandler_JoinRoomNotify;
import com.chatroom.net.client.msgHandlers.ClientMessageHandler_DeleteRoomNotify;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static java.util.Comparator.comparing;

/**
 * Created by William Sims on 7/8/2015.
 * Main class for the client side of the chatroom.  This singleton is used to communicate between the GUI and the
 * network layer.
 */
public class ChatClient {
    private Login loginGui;
    private List<Chatroom> chatrooms;
    private ChatroomGUI chatroomGUI;
    private List<ChatGUI> chatrooms_gui;
    private User user;
    private static ChatClient instance = null;

    private ClientConnection c;
    private List<MessageHandler> handlers;

    private ChatClient() {
     
        /* add any client msg handlers here */
        handlers = new ArrayList<>();

        handlers.add(new ClientMessageHandler_NewUserConf());
        handlers.add(new ClientMessageHandler_LoginConf());
        handlers.add(new ClientMessageHandler_LogoutConf());
        handlers.add(new ClientMessageHandler_NewRoomNotify());
        handlers.add(new ClientMessageHandler_JoinRoomNotify());
        handlers.add(new ClientMessageHandler_LeaveRoomNotify());
        handlers.add(new ClientMessageHandler_DeleteRoomNotify());
        handlers.add(new ClientMessageHandler_MessageNotify());
        handlers.add(new ClientMessageHandler_GetRoomsNotify());


        /* establish a network connection to the server */
        c = ClientConnectionManager.getInstance().AddClient("127.0.0.1", 10999);

        loginGui = new Login();
        chatrooms = new ArrayList<>();
        chatrooms_gui = new ArrayList<>();
        loginGui.setVisible(true);
        loginGui.setSize(300, 150);
        loginGui.setLocationRelativeTo(null);
    }

    public void Login(String user_name, String password) {
        // Create Message
        String prefix = "CR:C:LIN:";
        String suffix = user_name + ":" + password;

        // Send the login message
        c.sendMessage(prefix + suffix);
    }

    public void CreateUser(String user_name, String password) {
        // Create Message
        String prefix = "CR:C:NUSR:";
        String suffix = user_name + ":" + password;

        // Send the create user message
        c.sendMessage(prefix + suffix);
    }

    public void Logout() {
        // leave all chatrooms you are in.
        // TODO: Handle this on the server side?
        for (int i = chatrooms.size() - 1; i >= 0; i--) {
            LeaveChatroom(chatrooms.get(i).getName());
        }

        // Create Message
        String prefix = "CR:C:LOUT:";
        String suffix = user.getUser_name();

        // Send the logout message
        c.sendMessage(prefix + suffix);
    }

    // This function creates the GUI that lists the chattrooms.
    public void createChatroomGui() {
        loginGui.dispose(); // close the login gui

        chatroomGUI = new ChatroomGUI();  // display the chat gui
        chatroomGUI.setLocationRelativeTo(null);
        chatroomGUI.hasFocus();
    }

    public void CreateChatroom(String name) {

        for (int i = 0; i < chatrooms.size(); i++) {
            if (chatrooms.get(i).getName().equals(name)) {
                chatroomGUI.pop_up("CHATROOM ALREADY EXISTS", "ERROR");
                return;
            }
        }

        // Create Message
        String prefix = "CR:C:NRM:";
        String suffix = name;

        // Send the login message
        c.sendMessage(prefix + suffix);
    }

    public void AddChatroom(String name){
        chatrooms.add(new Chatroom(name));

    }

    public void newChatroomAdded(String name) {
        AddChatroom(name);

        chatroomGUI.updateChatrooom();
    }

    public void JoinChatroom(String chatroom_name) {

        for (int i = 0; i < chatrooms_gui.size(); i++) {
            if (chatrooms_gui.get(i).getChatroomName().equals(chatroom_name)) {
                chatrooms_gui.get(i).hasFocus();
                chatrooms_gui.get(i).toFront();
                return;
            }
        }

        // Create Message
        String prefix = "CR:C:JRM:";
        String suffix = chatroom_name;

        // Send the login message
        c.sendMessage(prefix + suffix);
    }

    public void ChatroomJoined(String chatroom_name) {

        chatrooms_gui.add(new ChatGUI(chatroom_name));

        chatrooms_gui.get(chatrooms_gui.size() - 1).hasFocus();
    }

    public void LeaveChatroom(String chatroom_name) {

        int index_to_remove = -1;
        for(int i = 0; i < chatrooms_gui.size(); i++){
            if ( chatrooms_gui.get(i).getChatroomName().equals(chatroom_name))
                index_to_remove = i;
        }

        if (index_to_remove >= 0){
            chatrooms_gui.remove(index_to_remove);
        }

        // Create Message
        String prefix = "CR:C:LRM:";
        String suffix = chatroom_name;

        // Send the leave room message
        c.sendMessage(prefix + suffix);
    }

    public void SendMessage(String message, String chatroom_name) {
        displayMessage(message, chatroom_name);

        String prefix = "CR:C:CHAT:";
        String suffix = chatroom_name + ":" + message;

        c.sendMessage(prefix + suffix);
    }

    public void displayMessage(String message, String chatroom_name){
        // This will change to be done on the message recieved from the server
        for(int i = 0; i < chatrooms_gui.size(); i++){
            if ( chatrooms_gui.get(i).getChatroomName().equals(chatroom_name)) {
                chatrooms_gui.get(i).UpdateMessages(message);


            }
        }
    }

    public List<Chatroom> getChatrooms() {
        return chatrooms;
    }

    public User getUser() {
        return user;
    }

    public static ChatClient getInstance(){
        if ( instance == null )
            instance = new ChatClient();
        return instance;
    }

    // Called if we successfully logged in via create user
    // or login
    public void success(String user_name)
    {
        user = new User(user_name);

        String prefix = "CR:C:GETRMS";

        c.sendMessage(prefix);

        //createChatroomGui();
    }

    // Called if we failed logged in via create user
    // or login
    public void failed(String message, String title)
    {
        loginGui.pop_up(message, title);
    }

    public void removeChatroom(String room_name) {
        for(int i = 0; i < chatrooms.size(); i++){
            if ( chatrooms.get(i).getName().equals(room_name))
                chatrooms.remove(chatrooms.get(i));
        }

        chatroomGUI.updateChatrooom();
    }
}
