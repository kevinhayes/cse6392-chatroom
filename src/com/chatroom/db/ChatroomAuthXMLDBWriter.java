package com.chatroom.db;

import java.io.File;

import javax.swing.DefaultListModel;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

/* 
 * Example XML:
 * 
 * <?xml version="1.0" encoding="UTF-8" standalone="no"?>
 * <ChatroomRegistration>
 *     <User Name="John Doe" >
 *         <Authentication Password="P@$$w0rd" /Authentication>
 *     </User>
 * </ChatroomRegistration>
 * 
 */

/**
 *  Class for writing user authentication data to xml file
 */
public class ChatroomAuthXMLDBWriter
{
    private Document doc;
    private Element root;
    
    private String filePath;
    
    /**
     *  Constructor that takes the file path as an argument.
     *  Creates a Document object using a DocumentBuilder factory object
     *  newDocumentBuilder().  
     *  The root xml element is created and appended to the Document as 
     *  the root node or element.
     *  
     * Note: The Document Object Model (DOM) is an API that represents the entire document 
     *  (loaded into memory) as a tree of node objects representing the document's contents. 
     */
    public ChatroomAuthXMLDBWriter (String path) throws ParserConfigurationException
    {
        filePath = path;
        
        try {
            /** Get the DOM Builder Factory */
            DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance ();
            /** Get the DOM Builder */
            DocumentBuilder docBuilder = docFactory.newDocumentBuilder ();
            /** create the Document object to write the xml to */
            doc = docBuilder.newDocument ();
            
            /** Create the root element */
            root = doc.createElement ("ChatroomRegistration");
            /** write it to the doc */
            doc.appendChild (root);

        } catch (ParserConfigurationException pce) {
            pce.printStackTrace ();
        } 
    }
    
    /**
     *  File writer that uses Transformer to process XML 
     *  and write the transformation output.
     */
    public void writeToFile () throws TransformerConfigurationException
    {
      try {
        /** Instantiate the transformer factory */
        TransformerFactory transformerFactory = TransformerFactory.newInstance ();
        /** Instantiate the transformer object */
        Transformer transformer = transformerFactory.newTransformer ();
        /** Tell transformer to indent.  This ended up putting elements
         * and attributes on a new line versus the entire document in one line.  
         * However, the desire was to have indentation per parent/child relationships
         * in the xml file but this option did not deliver that level of "pretty print."
         */
        transformer.setOutputProperty (OutputKeys.INDENT, "yes");
        transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "2");

        /** Create the DOMsource object.  This and a StreamResult
         * object will be used to write the xml to some file path.
         */
        DOMSource source = new DOMSource (doc);
        StreamResult result = new StreamResult (new File (filePath));

        /** Output to console for testing */
        // StreamResult result = new StreamResult(System.out);

        /** Write the file */
        transformer.transform (source, result);
        } catch (TransformerException tfe) {
            tfe.printStackTrace ();
      }
    }
   
    /**
     *  writes User data in xml format
     */
    public void writeUser ( User u ) 
    {
      int i, j;
      
      /** Create the User element */
      Element userElement = doc.createElement ("User");
      /** Append this as a child to the document root element <ChatroomRegistration> */
      root.appendChild (userElement);

      // shorter method
      // userElement.setAttribute("Name", c.getName());
      /** Create the "Name" attribute to the element Class */
      Attr userNameAttribute = doc . createAttribute( "Name" );
      userNameAttribute . setValue( u . getUser_name( ) );
      userElement . setAttributeNode( userNameAttribute );
 
      /** Create the <Authentication> element */
      Element authenticationElement = doc . createElement( "Authentication" );
      userElement . appendChild( authenticationElement );
      
      /** Create the "Password" attribute to the element <Authentication> */
      Attr passwordAttribute = doc . createAttribute( "Password" );
      passwordAttribute . setValue( u . getUser_pass( ) );
      authenticationElement . setAttributeNode( passwordAttribute );
      
    }
}
