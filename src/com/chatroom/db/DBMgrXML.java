package com.chatroom.db;

import com.chatroom.db.MD5;

import java.util.Vector;
import java.util.Iterator;

import java.io.*;
import java.nio.file.*;

import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.parsers.ParserConfigurationException;
import org.xml.sax.SAXException;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class DBMgrXML 
{
  private static DBMgrXML instance = null;
  private static ChatroomAuthXMLDBLoader xmlFileLoader = null;
  private static ChatroomAuthXMLDBWriter xmlFileWriter = null;
  private static String filePath = null;
  private static Vector<User> users = null;
  private static boolean initialized = false;

  private static Lock _mutex;

  private static MD5 encryptMD5;

  private DBMgrXML( )
  {
    users = new Vector<User>( );
    _mutex = new ReentrantLock( true );

    encryptMD5 = new MD5( );
  }

  public static DBMgrXML getInstance( )
  {
    if( instance == null )
      instance = new DBMgrXML( );

    return instance;
  }

  public static boolean initialize( )
  {
    if( !initialized )
    {
      if( setFilePath( "path.txt" ) )
      {
        if( doesFileExist( filePath ) )
        {
          try {
            xmlFileLoader = new ChatroomAuthXMLDBLoader( filePath );
          }
          catch( IOException e ) {
              e . printStackTrace( );
          }
          catch (ParserConfigurationException pce) {
            pce.printStackTrace ();
          }
          catch (SAXException se) {
            se.printStackTrace ();
          }
        }
        else
          System.out.println( "DBMgrXML could not find the db path configuration file" );
  
        try {
          xmlFileWriter = new ChatroomAuthXMLDBWriter( filePath );
        } catch (ParserConfigurationException pce) {
          pce.printStackTrace ();
        }
         
        initialized = true;

        return true;
      }
    }

    return false;
  }

  public static boolean doesFileExist( String file_path )
  {
    boolean result = false;

    Path path = Paths.get( file_path );

    if( Files . exists( path ) )
      result = true;

    return result;
  }

  public static void deleteDB( )
  {
    Path path = Paths . get( filePath );

    try {
      Files . deleteIfExists( path );
    }
    catch( IOException e ) {
      e . printStackTrace( );
    }
  }

  public static boolean setFilePath( String path )
  {
    if( doesFileExist( path ) )
    {
      try {
        // FileReader reads text files in the default encoding.
        FileReader fileReader = new FileReader(path);

        // Always wrap FileReader in BufferedReader.
        BufferedReader bufferedReader = new BufferedReader(fileReader);

        String line = null;
        while((line = bufferedReader.readLine()) != null) 
        {
          filePath = line;
        }    

        // Always close files.
        bufferedReader.close();            
 
        return true;
      }
      catch(FileNotFoundException ex) {
        System.out.println( "Unable to open file '" + path + "'");                
      }
      catch(IOException ex) {
        System.out.println( "Error reading file '" + path + "'");                   
        // ex.printStackTrace();
      }
    }
    else
      System.out.println( "File does not exist (" + path + ")" );

    return false;
  }

  public static boolean validateUser( String name, String pass )
  {
    _mutex . lock( );

    Iterator<User> it = users . iterator( );

    while( it . hasNext( ) )
    {
      User usr = it . next( );
      if( usr . getUser_name( ) . equals( name ) &&
          usr . getUser_pass( ) . equals( DBMgrXML . getInstance() . encryptUserPass( pass ) ) )
      {
        _mutex . unlock( );
        return true;
      }
    }

    _mutex . unlock( );

    return false;
  }

  public static boolean addUser( User u )
  {
    _mutex . lock( );

    Iterator<User> it = users . iterator( );

    while( it . hasNext( ) )
    {
      User usr = it . next( );
      if( usr . getUser_name( ) . equals( u . getUser_name( ) ) )
      {
        _mutex . unlock( );
        return false;
      }
    }

    users . add( u );

    _mutex . unlock( );

    return true;
  }

  public static String encryptUserPass( String password )
  {
    return encryptMD5 . crypt(password);
  }

  public static void writeDB( )
  {
    deleteDB();

    Iterator<User> it = users . iterator( );

    while( it . hasNext( ) )
    {
      User u = it . next( );
      xmlFileWriter.writeUser( u );
    }
    
    try {
      xmlFileWriter.writeToFile( );
    } catch (TransformerException tfe) {
        tfe.printStackTrace ();
    }
  }

  // Loop through all the users, if the user exists return false
  public boolean checkUserName(String name) {
    for( int i = 0; i < users.size(); i++) {
      if (users.get(i).getUser_name().equals(name)) {
        return false;
      }
    }
    return true;
  }
}
