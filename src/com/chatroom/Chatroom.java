package com.chatroom;

import com.chatroom.db.User;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by William Sims on 7/8/2015.
 * This class is utlized to keep a list of all active chatrooms that can be joined.  On the client side only utilized
 * to do joins and leaves.  On the server side this may have more information, i.e. the list of clients in the chatroom
 * for messaging.
 */
public class Chatroom {
    private String name;
    public  List<User> users;

    public Chatroom(String chatroom_name) {
        name = chatroom_name;
        users = new ArrayList<>();
    }
  
    public String getName(){ return name;}

    public int getHeadCount() { return users.size(); }

    public boolean addUser( User user )
    {
        for ( int i = 0; i < users.size(); i++){
            if(users.get(i).getUser_name().equals(user.getUser_name()))
                return false;
        }

        users.add(user);
        return true;
    }

    public void removeUser(User user){
        for ( int i = 0; i < users.size(); i++){
            if(users.get(i).getUser_name().equals(user.getUser_name())) {
                users.remove(i);
                return;
            }
        }
    }
}
