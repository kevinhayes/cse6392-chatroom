package com.chatroom;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by William Sims on 7/15/2015.
 * Contains the list of buttons to send the join chatrooms, also used to create new chatrooms and logout.
 */
public class ChatroomGUI extends JFrame{

    private JPanel panel = new JPanel();
    private JSplitPane split = new JSplitPane();
    private ScrollPane scrollPane = new ScrollPane();

    public ChatroomGUI()
    {
        super("Chatrooms");
        // removes the gui from the client
        this.addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                ChatClient.getInstance().Logout();
            }
        });

        JButton newChatroomButton = new JButton("New Chatroom");
        newChatroomButton.setBounds(10, 10, 120, 25);
        newChatroomButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
            	String chatroom_name = JOptionPane.showInputDialog("Chatroom Name", "");
            	ChatClient.getInstance().CreateChatroom(chatroom_name);
            	//

            	//

            }
        });
        panel.add(newChatroomButton);

        JButton logoutButton = new JButton("Logout");
        logoutButton.setBounds(100, 10, 80, 25);
        logoutButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                ChatClient.getInstance().Logout();
            }
        });
        panel.add(logoutButton);

        updateChatrooom();

        setVisible(true);
    }

    public void pop_up(String message, String title){
        JOptionPane.showMessageDialog(null, message, "InfoBox: " + title, JOptionPane.INFORMATION_MESSAGE);
    }

    public void updateChatrooom() {

        remove(split);

        // Create the list of chatroom buttons
        JPanel chats_panel = new JPanel();

        List<Chatroom> chatrooms = ChatClient.getInstance().getChatrooms();

        List<JButton> newButtons = new ArrayList<JButton>();
        for( int i = 0; i < chatrooms.size(); i++) {
            newButtons.add(new JButton(chatrooms.get(i).getName()));
            //newButtons.get(i).setBounds(100, 40, 80, 25);
            newButtons.get(i).setActionCommand(chatrooms.get(i).getName());
            newButtons.get(i).addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    ChatClient.getInstance().JoinChatroom(e.getActionCommand());
                }
            });
            chats_panel.add(newButtons.get(i));
        }

        chats_panel.setPreferredSize(new Dimension(150, 35 * chatrooms.size()));

        scrollPane.add(chats_panel);

        split.setOrientation(JSplitPane.VERTICAL_SPLIT);
        split.setTopComponent(panel);
        split.setBottomComponent(scrollPane);

        add(split);
        validate();
        repaint();
        pack();
    }
}
