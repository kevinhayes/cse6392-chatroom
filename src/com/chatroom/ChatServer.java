package com.chatroom;

import com.chatroom.db.User;
import com.chatroom.net.MessageHandler;
import com.chatroom.net.server.msgHandlers.*;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by William Sims on 7/8/2015.
 */
public class ChatServer {
    private List<Chatroom> chatrooms;
    private List<User> users;
    private static ChatServer instance = null;
    private List<MessageHandler> handlers;

    private ChatServer(){

        /* add any server msg handlers here */
        handlers = new ArrayList<>();

        handlers.add(new ServerMessageHandler_Chat( ));
        handlers.add(new ServerMessageHandler_NewUser( ));
        handlers.add(new ServerMessageHandler_Login( ));
        handlers.add(new ServerMessageHandler_NewRoom( ));
        handlers.add(new ServerMessageHandler_JoinRoom( ));
        handlers.add(new ServerMessageHandler_LeaveRoom( ));
        handlers.add(new ServerMessageHandler_Logout( ));
        handlers.add(new ServerMessageHandler_GetRooms());


        chatrooms = new ArrayList<>();
        users = new ArrayList<>();
    }

    public void addUser(User user)
    {
        users.add(user);
    }

    // Remove the user from active user list by name
    public void removeUser(String user){
        for ( int i = 0; i < users.size(); i++){
            if(users.get(i).getUser_name().equals(user)) {
                users.remove(i);
                return;
            }
        }
    }

    // Get a User object from a client network ID
    public User getUser(int id){
        for ( int i = 0; i < users.size(); i++){
            if(users.get(i).getUser_id() == id) {
                return users.get(i);
            }
        }
        return null;
    }

    public boolean addChatroom(Chatroom chatroom)
    {
        for( int i = 0; i < chatrooms.size(); i++)
        {
            if(chatrooms.get(i).getName().equals(chatroom.getName())) {
               return false;
            }
        }

        chatrooms.add(chatroom);

        return true;
    }

    public boolean joinChatroom(int id, String room_name)
    {
        for( int i = 0; i < chatrooms.size(); i++)
        {
            if(chatrooms.get(i).getName().equals(room_name)) {
              for( int j = 0; j < chatrooms.get(i).users.size(); j++ )
              {
                if( chatrooms.get(i).users.get(j).getUser_id() == id )
                {
                  /* already in the chatroom */
                  return false;
                }
              }
              chatrooms.get(i).addUser( getUser(id) );
            }
        }
        return true;
    }


    public boolean leaveChatroom(int id, String room_name)
    {
        for( int i = 0; i < chatrooms.size(); i++)
        {
            if(chatrooms.get(i).getName().equals(room_name)) {
              for( int j = 0; j < chatrooms.get(i).users.size(); j++ )
              {
                if( chatrooms.get(i).users.get(j).getUser_id() == id )
                {
                  chatrooms.get(i).removeUser( getUser(id) );
                  return true;
                }
              }
            }
        }
        return false;
    }

    public boolean ChatroomEmpty(String chatroom){
        for( int i = 0; i < chatrooms.size(); i++)
        {
            if( chatrooms.get(i).getName().equals(chatroom) &&
                chatrooms.get(i).getHeadCount() == 0      )
            {
                chatrooms.remove(chatrooms.get(i));
                return true;
            }
        }
        return false;
    }


    public static ChatServer getInstance(){
        if ( instance == null )
            instance = new ChatServer();
        return instance;
    }

    public List<Chatroom> getChatrooms() {
        return chatrooms;
    }
}
